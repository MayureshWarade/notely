﻿using NotelyApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotelyApp.Repository
{
    public class NoteRepository : INoteRepository
    {
        private readonly List<NoteModel> _notes;
        public NoteRepository()
        {
            _notes = new List<NoteModel>();
        }
        public NoteModel FindNoteById(Guid id)
        {
            var note = _notes.Find(n => n.Id == id);
            return note;
        }
        public IEnumerable<NoteModel> GetAllNotes()
        {
            return _notes;
        }
        public void SaveNote(NoteModel note)
        {
            _notes.Add(note);
        }
        public void DeleteNote(NoteModel note)
        {
            _notes.Remove(note);
        }
    }
}
